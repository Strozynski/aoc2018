use std::fs;
use std::collections::HashSet;

fn part_1(input: &str) -> i32 {
    input.lines().map(|l| l.parse::<i32>().unwrap()).sum()
}

fn part_2(input: &str) -> i32 {
    let mut frequencies :HashSet<i32> = HashSet::new();
    frequencies.insert(0);
    let mut current_freq = 0;
    let mut process_freq = |f :&i32| -> bool {
        current_freq += f;
        if frequencies.contains(&current_freq) {
            return false;
        }
        else {
            frequencies.insert(current_freq);
        }
        return true
    };

    input.lines().map(|l| l.parse::<i32>().unwrap()).cycle().take_while(|f| process_freq(f)).count();

    current_freq
}

fn main() {
    let content = fs::read_to_string("input").expect("file not found");
    let content = content.trim();
    print!("Result1: {}\n", part_1(&content));
    print!("Result2: {}\n", part_2(&content));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_1() {
        assert_eq!(3, part_1("+1\n+1\n+1\n"));
        assert_eq!(0, part_1("+1\n+1\n-2\n"));
        assert_eq!(-6, part_1("-1\n-2\n-3\n"));
    }
    #[test]
    fn test_part_2() {
        assert_eq!(0, part_2("+1\n-1\n"));
        assert_eq!(10, part_2("+3\n+3\n+4\n-2\n-4\n"));
        assert_eq!(14, part_2("+7\n+7\n-2\n-7\n-4\n"));
    }
}
